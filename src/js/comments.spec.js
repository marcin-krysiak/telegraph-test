const Comments = require("./comments");

describe( 'Comments', () => {
  jest.spyOn(document, 'getElementById').mockImplementation(() => ({
    classList: {
      add: () => null,
      remove: () => null
    }
  }));

  const populateCommentsMock = jest.spyOn(Comments.prototype, 'populateComments').mockImplementation(() => null); // 4
  const commentsMock = [
    {
      "id": 1,
      "date": "2019-04-23T19:26:41.511Z",
      "name": "Dawud Esparza",
      "body": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In sed gravida orci.",
      "likes": 4
    },
    {
      "id": 2,
      "date": "2019-04-23T22:26:43.511Z",
      "name": "Lennie Wainwright",
      "body": "Quisque maximus augue ut ex tincidunt sodales. Nullam interdum consectetur mi at pellentesque.",
      "likes": 33
    }
  ]

  describe( 'sortByLikes', () => {
    it('should sort comment by number of likes in descending order', async () => {
      const commentsController = new Comments();
      commentsController.sortByLikes(commentsMock);

      expect(populateCommentsMock).toHaveBeenCalledTimes(1);
      expect(populateCommentsMock).toHaveBeenCalledWith([
        {
          "id": 2,
          "date": "2019-04-23T22:26:43.511Z",
          "name": "Lennie Wainwright",
          "body": "Quisque maximus augue ut ex tincidunt sodales. Nullam interdum consectetur mi at pellentesque.",
          "likes": 33
        }, {
          "id": 1,
          "date": "2019-04-23T19:26:41.511Z",
          "name": "Dawud Esparza",
          "body": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In sed gravida orci.",
          "likes": 4
        }
      ]);
    });
  });

  describe( 'sortByDate', () => {
    it('should sort comment by date in descending order', async () => {
      const commentsController = new Comments();
      commentsController.sortByDate(commentsMock);

      expect(populateCommentsMock).toHaveBeenCalledTimes(1);
      expect(populateCommentsMock).toHaveBeenCalledWith([
        {
          "id": 2,
          "date": "2019-04-23T22:26:43.511Z",
          "name": "Lennie Wainwright",
          "body": "Quisque maximus augue ut ex tincidunt sodales. Nullam interdum consectetur mi at pellentesque.",
          "likes": 33
        }, {
          "id": 1,
          "date": "2019-04-23T19:26:41.511Z",
          "name": "Dawud Esparza",
          "body": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In sed gravida orci.",
          "likes": 4
        }
      ]);
    });
  });

  afterEach(() => {
    populateCommentsMock.mockClear();
  })
});