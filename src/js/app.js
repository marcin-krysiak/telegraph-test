const Utils = require('./utils');
const Comments = require('./Comments');

new Utils();

document.addEventListener("DOMContentLoaded", async function(event) {
  // run when DOM is loaded and ready
  const commentsController = new Comments();
  const comments = await commentsController.loadComments() || [];
  if (comments.length) {
    window.sortCommentsByLikes = () => commentsController.sortByLikes(comments);
    window.sortCommentsByDate = () => commentsController.sortByDate(comments);

    commentsController.populateComments(comments)
  }
});
